/* 
 * File:   core.h
 * Author: tolyan
 *
 * Created on 6 ???? 2015 ?., 12:46
 */

#ifndef CORE_H
#define	CORE_H

#include <stdint.h>

void initCore();
void checkChanelsAlive();
void CalcP();
void CalcT();
void genPrescalerPostscalerPlank(uint32_t Kd, uint8_t* PrescalerVal,
        uint8_t *postscalerVal, uint8_t *plank);

#endif	/* CORE_H */

