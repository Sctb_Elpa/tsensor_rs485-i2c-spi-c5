/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <stdlib.h>

/************************************************************************/
/* Stimul on T1CKI and SOSCI/SOSCO not working!                         */
/* Timer0 not generate interrupts in 16-bit mode (simulator bug?)       */
/************************************************************************/

#include <string.h>
#include <math.h>

#include "dataModel.h"

#include "FreqMeters.h"

/************************************************************************/
/* T0 8bit - Temperature                                                */
/* T3 16bit - Master freq                                               */
/************************************************************************/

volatile uint16_t RefFreqTimer_Ovf;

struct mesureCounter temperatureData;

union u16bitCounter TempTimerTarget_new;

uint8_t TempAliveCounter = ALIVE_COUNTER;

void FreqMetersEnable() {
    if ((CoilsBuf.names.TemperatureEnabled) &&
            (!CoilsBuf.names.PowerSaveMode)) {
        /* After programm reset T3CON in illegal state but T3CONbits.TMR3ON == 1*/
        PMD1bits.TMR3MD = 0;

        IPR2bits.TMR3IP = 0;
        PIR2bits.TMR3IF = 0;
        PIE2bits.TMR3IE = 1; // no interrupt

        T3CON = 0;
        T3CONbits.RD16 = 1; // 16-bit mode
        T3CONbits.TMR3CS = MASTER_CLOCK_FAST;
        T3CONbits.TMR3ON = 1; // enable

        if (CoilsBuf.names.TemperatureEnabled) {
            memset(&temperatureData, 0, sizeof (struct mesureCounter));

            PMD1bits.TMR0MD = 0;

            INTCON2bits.TMR0IP = FREQMETER_ISR_PRIO;
            INTCONbits.TMR0IF = 0;
            INTCONbits.TMR0IE = 1;

            T0CON = 0;
            T0CONbits.T0CS = 1; // T0CKI clock source
            T0CONbits.T08BIT = 0; // Force 16-bit mode
            T0CONbits.PSA = 1; // Timer0 clock input bypasses prescaler
            //T0CONbits.T0PS = 0b110;
        } else {
            T0CONbits.TMR0ON = 0;
            INTCONbits.TMR0IE = 0;

            PMD1bits.TMR0MD = 1;
        }
    } else {
        T0CONbits.TMR0ON = 0;
        T1CONbits.TMR1ON = 0;
        T3CONbits.TMR3ON = 0;

        PIE2bits.TMR3IE = 0;
        PIE1bits.TMR1IE = 0;
        INTCONbits.TMR0IE = 0;

        /* power down */
        PMD1bits.TMR0MD = 1;
        PMD1bits.TMR1MD = 1;
        PMD1bits.TMR3MD = 1;
    }
}
