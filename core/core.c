/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

#include <string.h>
#include <stdlib.h>

#include "mb.h"
#include "FreqMeters.h"
#include "dataModel.h"
#include "system.h"
#include "shaduler.h"
#include "user.h"

#include "core.h"

#define SUCCESS_LOOPS_NEEDED            3
#define CALC_OUTPUT_VALUE       1

static unsigned long maxU = ~0;
#define NAN  	(*((float*)&maxU))

void initCore() {
    InitDataModel();
    FreqMetersEnable();
    enableShaduler(1);
}

static void RestartTempCounter(uint32_t target) {
    TempTimerTarget_new.value = target;
    temperatureData.work = TempTimerTarget_new.s.Extantion;
    TempTimerTarget_new.s.REGISTER = (~TempTimerTarget_new.s.REGISTER) + 1;
    TMR0H = 0xff;
    NOP();
    TMR0L = 0xf0;
    T0CONbits.TMR0ON = 1; // start new cycle
    TempAliveCounter = ALIVE_COUNTER;
}

void checkChanelsAlive() {
#if 1
    DiscretInputsBuf.names.TemperatureChanelFailure = 0;
    if (CoilsBuf.names.PowerSaveMode) {
        // no mesure - no errors ;)
        DiscretInputsBuf.names.TemperatureChanelFailure = 0;
        DiscretInputsBuf.names.CryticalTemperature = false;
        RegInputBuf.names.Ft = 0.0;
        RegInputBuf.names.Temperature = NAN;
    } else {
        if (CoilsBuf.names.TemperatureEnabled) {
            if (!TempAliveCounter) {
                DiscretInputsBuf.names.TemperatureChanelFailure = 1; // error detected
                RegInputBuf.names.Temperature = NAN;
                RegInputBuf.names.Ft = 0.0;
                
                // force stop and restart
                T0CONbits.TMR0ON = 0;
                RestartTempCounter(1);
            } else {
                --TempAliveCounter;
            }
            DiscretInputsBuf.names.CryticalTemperature =
                    RegInputBuf.names.Temperature >= CRITICAL_TEMPERATURE;
        } else {
            DiscretInputsBuf.names.TemperatureChanelFailure = 0;
            RegInputBuf.names.Ft = 0.0;
            RegInputBuf.names.Temperature = NAN;
        }
    }
#endif
}

uint32_t calcTargetvalue(uint32_t F, uint16_t mesureTime_ms) {
    uint32_t res = (F * (uint32_t)mesureTime_ms) / 1000;
    return res ? res : 1;
}

static uint32_t resettarget(uint16_t measureTime) {
    return calcTargetvalue((uint32_t) START_FREQ,  measureTime);
}

static bool detectCatcheerror(uint16_t val) {
    return (val < 0xff);
}

static bool detectCatchErrors(union u16bitCounter data[2]) {
    return detectCatcheerror(data[0].value) || detectCatcheerror(data[1].value);
}

static uint32_t getResult(union u16bitCounter masterFreqData[2]) {
    uint32_t result = masterFreqData[1].value -  masterFreqData[0].value;
    if (result & (1ul << (sizeof (result) * 8 - 1)))
        result = (1ul << (sizeof (result) * 8 - 1)) -
            masterFreqData[0].value + masterFreqData[1].value;
    return result;
}

static uint32_t CalcNewTargetValue(bool isError, float target,
        float* F, uint8_t* Ready, uint16_t MesureTime, uint32_t startValue, 
        uint32_t *result) 
{
    if ((*result != startValue) && (!isError)) {
        *F = MASTER_CLOCK_HZ / ((float) (*result)) * target;
        *result = calcTargetvalue((uint32_t) *F,  MesureTime);
        if (*Ready < SUCCESS_LOOPS_NEEDED)
            (*Ready)++;
    } else {
        *result = resettarget(MesureTime);
        *Ready = 0;
    }
    
    return *result;
}

/* DEBUGER shows, what TMR0H fails to write, 8-bit mode set */
void CalcP() {
    if ((!T0CONbits.TMR0ON) &&
            CoilsBuf.names.TemperatureEnabled &&
            !CoilsBuf.names.PowerSaveMode) {
        uint32_t result;
        static uint32_t T_counter_target = 0;
        static uint8_t Redy = 0;
        float Ft;

        bool err = detectCatchErrors(temperatureData.masterFreqData);
        if (err && T_counter_target) {
            RestartTempCounter(T_counter_target);
            return;
        }

        result = getResult(temperatureData.masterFreqData);
        
        T_counter_target =
                CalcNewTargetValue(
                DiscretInputsBuf.names.TemperatureChanelFailure, 
                T_counter_target, &Ft, &Redy, RegHoldingBuf.names.TemperatureMesureTime,
                temperatureData.masterFreqData[0].value, &result);
        
        RestartTempCounter(T_counter_target);

#if CALC_OUTPUT_VALUE
        // calc
        if (!DiscretInputsBuf.names.TemperatureChanelFailure
                && (Redy == SUCCESS_LOOPS_NEEDED)) {
            // filter nidddles
            struct sTemperatureCoeffs TemperatureCoeffs;
            memcpy(&TemperatureCoeffs, &RegHoldingBuf.names.T_coeffs,
                    sizeof (struct sTemperatureCoeffs));
            float TempF_minus_Ft0 = Ft - TemperatureCoeffs.F0;
            float _temp = TempF_minus_Ft0;
            float Temperature_fSens = TemperatureCoeffs.T0;

            processTimeCrytical();

            for (unsigned char i = 0; i < 5; ++i, _temp *= TempF_minus_Ft0) {
                Temperature_fSens += TemperatureCoeffs.C[i] * _temp;
            }

            RegInputBuf.names.Temperature = Temperature_fSens;
            RegInputBuf.names.Ft = Ft;
        }
#else
        RegInputBuf.names.Ft = Fp;
        RegInputBuf.names.Temperature = result;
#endif
    }
}

void genPrescalerPostscalerPlank(uint32_t Kd, uint8_t* PrescalerVal,
        uint8_t *postscalerVal, uint8_t *plank) {
    static const uint8_t prescalers[] = {1, 4, 16};
    uint8_t postscaler;
    uint8_t i;
    uint8_t _plank = 0xff;

    for (i = 0; i < sizeof (prescalers); ++i)
        for (postscaler = 1; postscaler <= 16; ++postscaler) {
            uint16_t need_dev = Kd / (prescalers[i] * postscaler);
            if (need_dev < 256) {
                _plank = need_dev;
                goto __end_genPrescalerPostscalerStart;
            }
        }

__end_genPrescalerPostscalerStart:
    *PrescalerVal = i;
    *postscalerVal = postscaler - 1;
    *plank = _plank;
}