#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-XC8_18F25K80.mk)" "nbproject/Makefile-local-XC8_18F25K80.mk"
include nbproject/Makefile-local-XC8_18F25K80.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=XC8_18F25K80
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=${DISTDIR}/TSensor_RS485-I2C-SPI-C5.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=${DISTDIR}/TSensor_RS485-I2C-SPI-C5.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=core/core.c core/dataModel.c core/FreqMeters.c core/shaduler.c io/i2c/i2c_io.c io/RS485/modbus/functions/mbfunccoils.c io/RS485/modbus/functions/mbfuncdiag.c io/RS485/modbus/functions/mbfuncdisc.c io/RS485/modbus/functions/mbfuncholding.c io/RS485/modbus/functions/mbfuncinput.c io/RS485/modbus/functions/mbfuncother.c io/RS485/modbus/functions/mbutils.c io/RS485/modbus/rtu/mbcrc.c io/RS485/modbus/rtu/mbrtu.c io/RS485/modbus/mb.c io/RS485/port/critical.c io/RS485/port/portevent.c io/RS485/port/portserial.c io/RS485/port/porttimer.c io/spi/spi_io.c configuration_bits.c interrupts.c main.c system.c user.c dist/XC8_18F25K80/production/version.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/core/core.p1 ${OBJECTDIR}/core/dataModel.p1 ${OBJECTDIR}/core/FreqMeters.p1 ${OBJECTDIR}/core/shaduler.p1 ${OBJECTDIR}/io/i2c/i2c_io.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1 ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1 ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1 ${OBJECTDIR}/io/RS485/modbus/mb.p1 ${OBJECTDIR}/io/RS485/port/critical.p1 ${OBJECTDIR}/io/RS485/port/portevent.p1 ${OBJECTDIR}/io/RS485/port/portserial.p1 ${OBJECTDIR}/io/RS485/port/porttimer.p1 ${OBJECTDIR}/io/spi/spi_io.p1 ${OBJECTDIR}/configuration_bits.p1 ${OBJECTDIR}/interrupts.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/system.p1 ${OBJECTDIR}/user.p1 ${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/core/core.p1.d ${OBJECTDIR}/core/dataModel.p1.d ${OBJECTDIR}/core/FreqMeters.p1.d ${OBJECTDIR}/core/shaduler.p1.d ${OBJECTDIR}/io/i2c/i2c_io.p1.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1.d ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1.d ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1.d ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1.d ${OBJECTDIR}/io/RS485/modbus/mb.p1.d ${OBJECTDIR}/io/RS485/port/critical.p1.d ${OBJECTDIR}/io/RS485/port/portevent.p1.d ${OBJECTDIR}/io/RS485/port/portserial.p1.d ${OBJECTDIR}/io/RS485/port/porttimer.p1.d ${OBJECTDIR}/io/spi/spi_io.p1.d ${OBJECTDIR}/configuration_bits.p1.d ${OBJECTDIR}/interrupts.p1.d ${OBJECTDIR}/main.p1.d ${OBJECTDIR}/system.p1.d ${OBJECTDIR}/user.p1.d ${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/core/core.p1 ${OBJECTDIR}/core/dataModel.p1 ${OBJECTDIR}/core/FreqMeters.p1 ${OBJECTDIR}/core/shaduler.p1 ${OBJECTDIR}/io/i2c/i2c_io.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1 ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1 ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1 ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1 ${OBJECTDIR}/io/RS485/modbus/mb.p1 ${OBJECTDIR}/io/RS485/port/critical.p1 ${OBJECTDIR}/io/RS485/port/portevent.p1 ${OBJECTDIR}/io/RS485/port/portserial.p1 ${OBJECTDIR}/io/RS485/port/porttimer.p1 ${OBJECTDIR}/io/spi/spi_io.p1 ${OBJECTDIR}/configuration_bits.p1 ${OBJECTDIR}/interrupts.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/system.p1 ${OBJECTDIR}/user.p1 ${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1

# Source Files
SOURCEFILES=core/core.c core/dataModel.c core/FreqMeters.c core/shaduler.c io/i2c/i2c_io.c io/RS485/modbus/functions/mbfunccoils.c io/RS485/modbus/functions/mbfuncdiag.c io/RS485/modbus/functions/mbfuncdisc.c io/RS485/modbus/functions/mbfuncholding.c io/RS485/modbus/functions/mbfuncinput.c io/RS485/modbus/functions/mbfuncother.c io/RS485/modbus/functions/mbutils.c io/RS485/modbus/rtu/mbcrc.c io/RS485/modbus/rtu/mbrtu.c io/RS485/modbus/mb.c io/RS485/port/critical.c io/RS485/port/portevent.c io/RS485/port/portserial.c io/RS485/port/porttimer.c io/spi/spi_io.c configuration_bits.c interrupts.c main.c system.c user.c dist/XC8_18F25K80/production/version.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

# The following macros may be used in the pre and post step lines
_/_=\\
ShExtension=.bat
Device=PIC18F25K80
ProjectDir="D:\Razrab\PIC\work\TSensor_RS485-I2C-SPI-C5"
ProjectName=TSensor_RS485-I2C-SPI-C5
ConfName=XC8_18F25K80
ImagePath="dist\XC8_18F25K80\${IMAGE_TYPE}\TSensor_RS485-I2C-SPI-C5.${IMAGE_TYPE}.${OUTPUT_SUFFIX}"
ImageDir="dist\XC8_18F25K80\${IMAGE_TYPE}"
ImageName="TSensor_RS485-I2C-SPI-C5.${IMAGE_TYPE}.${OUTPUT_SUFFIX}"
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IsDebug="true"
else
IsDebug="false"
endif

.build-conf:  .pre ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-XC8_18F25K80.mk ${DISTDIR}/TSensor_RS485-I2C-SPI-C5.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F25K80
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/core/core.p1: core/core.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/core" 
	@${RM} ${OBJECTDIR}/core/core.p1.d 
	@${RM} ${OBJECTDIR}/core/core.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/core/core.p1 core/core.c 
	@-${MV} ${OBJECTDIR}/core/core.d ${OBJECTDIR}/core/core.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/core/core.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/core/dataModel.p1: core/dataModel.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/core" 
	@${RM} ${OBJECTDIR}/core/dataModel.p1.d 
	@${RM} ${OBJECTDIR}/core/dataModel.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/core/dataModel.p1 core/dataModel.c 
	@-${MV} ${OBJECTDIR}/core/dataModel.d ${OBJECTDIR}/core/dataModel.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/core/dataModel.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/core/FreqMeters.p1: core/FreqMeters.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/core" 
	@${RM} ${OBJECTDIR}/core/FreqMeters.p1.d 
	@${RM} ${OBJECTDIR}/core/FreqMeters.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/core/FreqMeters.p1 core/FreqMeters.c 
	@-${MV} ${OBJECTDIR}/core/FreqMeters.d ${OBJECTDIR}/core/FreqMeters.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/core/FreqMeters.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/core/shaduler.p1: core/shaduler.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/core" 
	@${RM} ${OBJECTDIR}/core/shaduler.p1.d 
	@${RM} ${OBJECTDIR}/core/shaduler.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/core/shaduler.p1 core/shaduler.c 
	@-${MV} ${OBJECTDIR}/core/shaduler.d ${OBJECTDIR}/core/shaduler.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/core/shaduler.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/i2c/i2c_io.p1: io/i2c/i2c_io.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/i2c" 
	@${RM} ${OBJECTDIR}/io/i2c/i2c_io.p1.d 
	@${RM} ${OBJECTDIR}/io/i2c/i2c_io.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/i2c/i2c_io.p1 io/i2c/i2c_io.c 
	@-${MV} ${OBJECTDIR}/io/i2c/i2c_io.d ${OBJECTDIR}/io/i2c/i2c_io.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/i2c/i2c_io.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1: io/RS485/modbus/functions/mbfunccoils.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1 io/RS485/modbus/functions/mbfunccoils.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1: io/RS485/modbus/functions/mbfuncdiag.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1 io/RS485/modbus/functions/mbfuncdiag.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1: io/RS485/modbus/functions/mbfuncdisc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1 io/RS485/modbus/functions/mbfuncdisc.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1: io/RS485/modbus/functions/mbfuncholding.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1 io/RS485/modbus/functions/mbfuncholding.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1: io/RS485/modbus/functions/mbfuncinput.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1 io/RS485/modbus/functions/mbfuncinput.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1: io/RS485/modbus/functions/mbfuncother.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1 io/RS485/modbus/functions/mbfuncother.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1: io/RS485/modbus/functions/mbutils.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1 io/RS485/modbus/functions/mbutils.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.d ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1: io/RS485/modbus/rtu/mbcrc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/rtu" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1 io/RS485/modbus/rtu/mbcrc.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.d ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1: io/RS485/modbus/rtu/mbrtu.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/rtu" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1 io/RS485/modbus/rtu/mbrtu.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.d ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/mb.p1: io/RS485/modbus/mb.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/mb.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/mb.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/mb.p1 io/RS485/modbus/mb.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/mb.d ${OBJECTDIR}/io/RS485/modbus/mb.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/mb.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/port/critical.p1: io/RS485/port/critical.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/port" 
	@${RM} ${OBJECTDIR}/io/RS485/port/critical.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/port/critical.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/port/critical.p1 io/RS485/port/critical.c 
	@-${MV} ${OBJECTDIR}/io/RS485/port/critical.d ${OBJECTDIR}/io/RS485/port/critical.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/port/critical.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/port/portevent.p1: io/RS485/port/portevent.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/port" 
	@${RM} ${OBJECTDIR}/io/RS485/port/portevent.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/port/portevent.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/port/portevent.p1 io/RS485/port/portevent.c 
	@-${MV} ${OBJECTDIR}/io/RS485/port/portevent.d ${OBJECTDIR}/io/RS485/port/portevent.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/port/portevent.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/port/portserial.p1: io/RS485/port/portserial.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/port" 
	@${RM} ${OBJECTDIR}/io/RS485/port/portserial.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/port/portserial.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/port/portserial.p1 io/RS485/port/portserial.c 
	@-${MV} ${OBJECTDIR}/io/RS485/port/portserial.d ${OBJECTDIR}/io/RS485/port/portserial.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/port/portserial.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/port/porttimer.p1: io/RS485/port/porttimer.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/port" 
	@${RM} ${OBJECTDIR}/io/RS485/port/porttimer.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/port/porttimer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/port/porttimer.p1 io/RS485/port/porttimer.c 
	@-${MV} ${OBJECTDIR}/io/RS485/port/porttimer.d ${OBJECTDIR}/io/RS485/port/porttimer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/port/porttimer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/spi/spi_io.p1: io/spi/spi_io.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/spi" 
	@${RM} ${OBJECTDIR}/io/spi/spi_io.p1.d 
	@${RM} ${OBJECTDIR}/io/spi/spi_io.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/spi/spi_io.p1 io/spi/spi_io.c 
	@-${MV} ${OBJECTDIR}/io/spi/spi_io.d ${OBJECTDIR}/io/spi/spi_io.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/spi/spi_io.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/configuration_bits.p1: configuration_bits.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/configuration_bits.p1.d 
	@${RM} ${OBJECTDIR}/configuration_bits.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/configuration_bits.p1 configuration_bits.c 
	@-${MV} ${OBJECTDIR}/configuration_bits.d ${OBJECTDIR}/configuration_bits.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/configuration_bits.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/interrupts.p1: interrupts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/interrupts.p1.d 
	@${RM} ${OBJECTDIR}/interrupts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/interrupts.p1 interrupts.c 
	@-${MV} ${OBJECTDIR}/interrupts.d ${OBJECTDIR}/interrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/interrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/system.p1: system.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/system.p1.d 
	@${RM} ${OBJECTDIR}/system.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/system.p1 system.c 
	@-${MV} ${OBJECTDIR}/system.d ${OBJECTDIR}/system.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/system.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/user.p1: user.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/user.p1.d 
	@${RM} ${OBJECTDIR}/user.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/user.p1 user.c 
	@-${MV} ${OBJECTDIR}/user.d ${OBJECTDIR}/user.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/user.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1: dist/XC8_18F25K80/production/version.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/dist/XC8_18F25K80/production" 
	@${RM} ${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1.d 
	@${RM} ${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=pickit3    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1 dist/XC8_18F25K80/production/version.c 
	@-${MV} ${OBJECTDIR}/dist/XC8_18F25K80/production/version.d ${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/core/core.p1: core/core.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/core" 
	@${RM} ${OBJECTDIR}/core/core.p1.d 
	@${RM} ${OBJECTDIR}/core/core.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/core/core.p1 core/core.c 
	@-${MV} ${OBJECTDIR}/core/core.d ${OBJECTDIR}/core/core.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/core/core.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/core/dataModel.p1: core/dataModel.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/core" 
	@${RM} ${OBJECTDIR}/core/dataModel.p1.d 
	@${RM} ${OBJECTDIR}/core/dataModel.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/core/dataModel.p1 core/dataModel.c 
	@-${MV} ${OBJECTDIR}/core/dataModel.d ${OBJECTDIR}/core/dataModel.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/core/dataModel.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/core/FreqMeters.p1: core/FreqMeters.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/core" 
	@${RM} ${OBJECTDIR}/core/FreqMeters.p1.d 
	@${RM} ${OBJECTDIR}/core/FreqMeters.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/core/FreqMeters.p1 core/FreqMeters.c 
	@-${MV} ${OBJECTDIR}/core/FreqMeters.d ${OBJECTDIR}/core/FreqMeters.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/core/FreqMeters.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/core/shaduler.p1: core/shaduler.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/core" 
	@${RM} ${OBJECTDIR}/core/shaduler.p1.d 
	@${RM} ${OBJECTDIR}/core/shaduler.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/core/shaduler.p1 core/shaduler.c 
	@-${MV} ${OBJECTDIR}/core/shaduler.d ${OBJECTDIR}/core/shaduler.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/core/shaduler.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/i2c/i2c_io.p1: io/i2c/i2c_io.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/i2c" 
	@${RM} ${OBJECTDIR}/io/i2c/i2c_io.p1.d 
	@${RM} ${OBJECTDIR}/io/i2c/i2c_io.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/i2c/i2c_io.p1 io/i2c/i2c_io.c 
	@-${MV} ${OBJECTDIR}/io/i2c/i2c_io.d ${OBJECTDIR}/io/i2c/i2c_io.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/i2c/i2c_io.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1: io/RS485/modbus/functions/mbfunccoils.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1 io/RS485/modbus/functions/mbfunccoils.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbfunccoils.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1: io/RS485/modbus/functions/mbfuncdiag.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1 io/RS485/modbus/functions/mbfuncdiag.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdiag.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1: io/RS485/modbus/functions/mbfuncdisc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1 io/RS485/modbus/functions/mbfuncdisc.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncdisc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1: io/RS485/modbus/functions/mbfuncholding.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1 io/RS485/modbus/functions/mbfuncholding.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncholding.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1: io/RS485/modbus/functions/mbfuncinput.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1 io/RS485/modbus/functions/mbfuncinput.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncinput.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1: io/RS485/modbus/functions/mbfuncother.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1 io/RS485/modbus/functions/mbfuncother.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.d ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbfuncother.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1: io/RS485/modbus/functions/mbutils.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/functions" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1 io/RS485/modbus/functions/mbutils.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.d ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/functions/mbutils.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1: io/RS485/modbus/rtu/mbcrc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/rtu" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1 io/RS485/modbus/rtu/mbcrc.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.d ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/rtu/mbcrc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1: io/RS485/modbus/rtu/mbrtu.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus/rtu" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1 io/RS485/modbus/rtu/mbrtu.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.d ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/rtu/mbrtu.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/modbus/mb.p1: io/RS485/modbus/mb.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/modbus" 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/mb.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/modbus/mb.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/modbus/mb.p1 io/RS485/modbus/mb.c 
	@-${MV} ${OBJECTDIR}/io/RS485/modbus/mb.d ${OBJECTDIR}/io/RS485/modbus/mb.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/modbus/mb.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/port/critical.p1: io/RS485/port/critical.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/port" 
	@${RM} ${OBJECTDIR}/io/RS485/port/critical.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/port/critical.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/port/critical.p1 io/RS485/port/critical.c 
	@-${MV} ${OBJECTDIR}/io/RS485/port/critical.d ${OBJECTDIR}/io/RS485/port/critical.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/port/critical.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/port/portevent.p1: io/RS485/port/portevent.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/port" 
	@${RM} ${OBJECTDIR}/io/RS485/port/portevent.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/port/portevent.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/port/portevent.p1 io/RS485/port/portevent.c 
	@-${MV} ${OBJECTDIR}/io/RS485/port/portevent.d ${OBJECTDIR}/io/RS485/port/portevent.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/port/portevent.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/port/portserial.p1: io/RS485/port/portserial.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/port" 
	@${RM} ${OBJECTDIR}/io/RS485/port/portserial.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/port/portserial.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/port/portserial.p1 io/RS485/port/portserial.c 
	@-${MV} ${OBJECTDIR}/io/RS485/port/portserial.d ${OBJECTDIR}/io/RS485/port/portserial.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/port/portserial.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/RS485/port/porttimer.p1: io/RS485/port/porttimer.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/RS485/port" 
	@${RM} ${OBJECTDIR}/io/RS485/port/porttimer.p1.d 
	@${RM} ${OBJECTDIR}/io/RS485/port/porttimer.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/RS485/port/porttimer.p1 io/RS485/port/porttimer.c 
	@-${MV} ${OBJECTDIR}/io/RS485/port/porttimer.d ${OBJECTDIR}/io/RS485/port/porttimer.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/RS485/port/porttimer.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/io/spi/spi_io.p1: io/spi/spi_io.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/io/spi" 
	@${RM} ${OBJECTDIR}/io/spi/spi_io.p1.d 
	@${RM} ${OBJECTDIR}/io/spi/spi_io.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/io/spi/spi_io.p1 io/spi/spi_io.c 
	@-${MV} ${OBJECTDIR}/io/spi/spi_io.d ${OBJECTDIR}/io/spi/spi_io.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/io/spi/spi_io.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/configuration_bits.p1: configuration_bits.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/configuration_bits.p1.d 
	@${RM} ${OBJECTDIR}/configuration_bits.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/configuration_bits.p1 configuration_bits.c 
	@-${MV} ${OBJECTDIR}/configuration_bits.d ${OBJECTDIR}/configuration_bits.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/configuration_bits.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/interrupts.p1: interrupts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/interrupts.p1.d 
	@${RM} ${OBJECTDIR}/interrupts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/interrupts.p1 interrupts.c 
	@-${MV} ${OBJECTDIR}/interrupts.d ${OBJECTDIR}/interrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/interrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/system.p1: system.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/system.p1.d 
	@${RM} ${OBJECTDIR}/system.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/system.p1 system.c 
	@-${MV} ${OBJECTDIR}/system.d ${OBJECTDIR}/system.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/system.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/user.p1: user.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/user.p1.d 
	@${RM} ${OBJECTDIR}/user.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/user.p1 user.c 
	@-${MV} ${OBJECTDIR}/user.d ${OBJECTDIR}/user.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/user.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1: dist/XC8_18F25K80/production/version.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/dist/XC8_18F25K80/production" 
	@${RM} ${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1.d 
	@${RM} ${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist -DXPRJ_XC8_18F25K80=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1 dist/XC8_18F25K80/production/version.c 
	@-${MV} ${OBJECTDIR}/dist/XC8_18F25K80/production/version.d ${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/dist/XC8_18F25K80/production/version.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${DISTDIR}/TSensor_RS485-I2C-SPI-C5.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} ${DISTDIR} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -m${DISTDIR}/TSensor_RS485-I2C-SPI-C5.${IMAGE_TYPE}.map  -D__DEBUG=1  --debugger=pickit3  -DXPRJ_XC8_18F25K80=$(CND_CONF)    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"        $(COMPARISON_BUILD)  -o${DISTDIR}/TSensor_RS485-I2C-SPI-C5.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} ${DISTDIR}/TSensor_RS485-I2C-SPI-C5.${IMAGE_TYPE}.hex 
	
	
else
${DISTDIR}/TSensor_RS485-I2C-SPI-C5.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} ${DISTDIR} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -m${DISTDIR}/TSensor_RS485-I2C-SPI-C5.${IMAGE_TYPE}.map  -DXPRJ_XC8_18F25K80=$(CND_CONF)    --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,+debug --addrqual=ignore --mode=pro -DF_CPU=10000000 -DUSE_PLL=0 -DMUTE_RESET -P -N255 -I"port" -I"core" -I"io/i2c" -I"io/RS485" -I"io/spi" -I"io/RS485/modbus/include" -I"io/RS485/modbus/rtu" -I"io/RS485/port" -I"." --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=+mcof,-elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     $(COMPARISON_BUILD)  -o${DISTDIR}/TSensor_RS485-I2C-SPI-C5.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
	
endif

.pre:
	@echo "--------------------------------------"
	@echo "User defined pre-build step: [${ProjectDir}\gen_version.py ${ProjectDir}\\${ImageDir}\\version.c ${ProjectDir}\version.c.in ${ProjectDir}\VERSION.txt]"
	@${ProjectDir}\gen_version.py ${ProjectDir}\\${ImageDir}\\version.c ${ProjectDir}\version.c.in ${ProjectDir}\VERSION.txt
	@echo "--------------------------------------"

# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${OBJECTDIR}
	${RM} -r ${DISTDIR}

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(wildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
