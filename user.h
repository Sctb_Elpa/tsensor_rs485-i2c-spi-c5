/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/


/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/


void InitApp(void);             /* I/O and Peripheral Initialization */
void UserMain();                /* user main loop body */
void applySettings();           /* apply settings what needs restart modules */
void Switch2I2C();   			/* select spi/i2c mode */
void SPIDetected();    			/* spi mode detect ISR */
void processTimeCrytical();		/* protocols state mashins and event loop */
void initMB();

typedef void (*p_ISR)(void);
extern p_ISR pINT0_ISR;
extern p_ISR SSP_ISR;