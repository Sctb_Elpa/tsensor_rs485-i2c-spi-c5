/* 
 * File:   spi_io.h
 * Author: tolyan
 *
 * Created on 12 ???? 2015 ?., 16:51
 */

#ifndef SPI_IO_H
#define	SPI_IO_H

#include <spi.h>

#define SPI_HIGH_PRIO		0

enum SPI_Mode
{
    spi_IDLE = 1 << 7,
    spi_CMD = 1 << 0,
    spi_ADDR = 1 << 1,
	spi_READY = 1 << 2,
	spi_DATA = 1 << 3,
    spi_WRITE = 1 << 4,
};

enum SPI_DataSegment
{
    spi_pHOLDING = 0,
    spi_pINPUTS = 1,
    spi_pCOILS = 2,
    spi_pDINPUTS = 3
};

void enable_spi(BOOL enable);
void SPI_ISR();
void SPI_Start_detected();

#endif	/* SPI_IO_H */

